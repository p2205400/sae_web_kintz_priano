# SAE WEB

## Description du Projet

Ce projet SAE a pour objectif de créer un site web de 3 à 4 pages sur le thème : "Les différents modes de déplacements doux pour venir ou se déplacer à l’intérieur de l’université". Le projet sera réalisé en binôme, avec des contributions à la fois dans la partie communication et le développement d'interfaces web.

### Livrables

- Document PDF avec les maquettes.
- Code source (dépôt git privé sur la forge) ou archive compressée.
- URL du site sur le serveur de l'IUT ou sur la forge

```
http://p2205400.pages.univ-lyon1.fr/sae_web_kintz_priano
```

### Échéance

Au plus tard le jeudi 21 décembre.

### Auteurs

- Nathan PRIANO
- Dylan Kintz
